package pl.ttpsc.zzp.fellowship;

import pl.ttpsc.zzp.fellowship.creatures.Creature;
import pl.ttpsc.zzp.fellowship.creatures.Dwarf;
import pl.ttpsc.zzp.fellowship.creatures.Elf;
import pl.ttpsc.zzp.fellowship.creatures.Hobbit;
import pl.ttpsc.zzp.fellowship.creatures.Maiar;
import pl.ttpsc.zzp.fellowship.creatures.Man;
import pl.ttpsc.zzp.fellowship.creatures.Valar;

import java.util.LinkedList;
import java.util.List;

class Fellowship {

    private List<Creature> members = new LinkedList<>();

    Fellowship() {
        this.members.add(new Gandalf());
        this.members.add(new Aragorn());
        this.members.add(new Boromir());
        this.members.add(new Legolas());
        this.members.add(new Gimli());
        this.members.add(new Frodo());
        this.members.add(new Samwise());
        this.members.add(new Merry());
        this.members.add(new Pippin());
    }

    List<Creature> getMembers() {
        return members;
    }

    private static class Gandalf extends Valar {

        @Override
        public String getName() {
            return "Gandalf";
        }
    }
    private static class Aragorn extends Man {

        @Override
        public String getName() {
            return "Aragorn";
        }
    }
    private static class Boromir extends Man {

        @Override
        public String getName() {
            return "Boromir";
        }
    }
    private static class Legolas extends Elf {

        @Override
        public String getName() {
            return "Legolas";
        }
    }
    private static class Gimli extends Dwarf {

        @Override
        public String getName() {
            return "Gimli";
        }
    }
    private static class Frodo extends Hobbit {

        @Override
        public String getName() {
            return "Frodo Baggins";
        }
    }
    private static class Samwise extends Hobbit {

        @Override
        public String getName() {
            return "Samwise Gamgee";
        }
    }
    private static class Merry extends Hobbit {

        @Override
        public String getName() {
            return "Meriadoc \"Merry\" Brandybuck";
        }
    }
    private static class Pippin extends Hobbit {

        @Override
        public String getName() {
            return "Peregrin \"Pippin\" Took";
        }
    }
}
