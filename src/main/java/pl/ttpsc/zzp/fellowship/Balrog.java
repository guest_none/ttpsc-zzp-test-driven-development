package pl.ttpsc.zzp.fellowship;

import pl.ttpsc.zzp.fellowship.creatures.Maiar;

public class Balrog extends Maiar {

    @Override
    public String getName() {
        return "Balrog";
    }
}