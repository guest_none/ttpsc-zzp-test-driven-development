package pl.ttpsc.zzp.fellowship;

import pl.ttpsc.zzp.fellowship.creatures.Valar;

public class Saruman extends Valar {

    @Override
    public String getName() {
        return "Saruman";
    }
}