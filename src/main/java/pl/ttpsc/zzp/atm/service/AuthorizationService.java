package pl.ttpsc.zzp.atm.service;

import pl.ttpsc.zzp.atm.model.BankAccount;
import pl.ttpsc.zzp.atm.model.PaymentCard;
import pl.ttpsc.zzp.atm.service.exceptions.AuthorizationException;
import pl.ttpsc.zzp.atm.service.exceptions.BannedPaymentCardException;

public interface AuthorizationService {
    BankAccount authenticate(PaymentCard card, String pin) throws AuthorizationException, BannedPaymentCardException;
}
