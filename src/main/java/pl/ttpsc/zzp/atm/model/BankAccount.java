package pl.ttpsc.zzp.atm.model;

import java.math.BigDecimal;
import java.util.List;

public class BankAccount {
    private String accountNumber;
    private BigDecimal balance;
    private List<PaymentCard> cards;

    public BankAccount(String accountNumber, BigDecimal baseBalance) {
        this.accountNumber = accountNumber;
        this.balance = baseBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
